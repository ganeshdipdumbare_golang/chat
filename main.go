package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	logging "gitlab.com/ganeshdipdumbare_golang/logging"
)

var logger = logging.Log
var connections = [](*websocket.Conn){}

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func removeFromSlice(conn *websocket.Conn) {
	fmt.Println("This function is called")
	tmpConnections := [](*websocket.Conn){}
	for _, v := range connections {
		if v == conn {
			continue
		}
		tmpConnections = append(tmpConnections, v)
	}
	connections = tmpConnections
}

func wshandler(w http.ResponseWriter, r *http.Request, name string) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	connections = append(connections, conn)
	if err != nil {
		logger.Error("Failed to set websocket upgrade: ", err)
		return
	}

	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		finalmsg := name + ":" + string(msg)
		for _, v := range connections {
			v.WriteMessage(t, []byte(finalmsg))
		}
	}
	removeFromSlice(conn)
}

func main() {
	router := gin.Default()

	router.GET("/ws/:name", func(c *gin.Context) {
		name := c.Param("name")
		wshandler(c.Writer, c.Request, name)
	})

	router.Run(":8000")
}
