module gitlab.com/ganeshdipdumbare_golang/chat

go 1.12

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/gorilla/websocket v1.4.0
	gitlab.com/ganeshdipdumbare_golang/logging v1.0.0
)
